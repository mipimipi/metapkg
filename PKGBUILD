# metapkg for mipi's Arch Linux installation
#
# Maintainer: Michael Picht <mipi@fsfe.org>

pkgbase=mipi
pkgname=(
  mipi-gnome
  mipi-apps-base
  mipi-apps-bluetooth
  mipi-apps-development
  mipi-apps-mobile
  mipi-apps-multimedia
)
pkgver=0.1.5
pkgrel=1
arch=(any)
license=(GPL3)
backup=("etc/fstab")
source=(
  dconf-00_extensions
  dconf-10_theming
  dconf-20_keybindings
  dconf-30_misc
  dconf-profile
  mipi-apps-base-any.systemd.preset
  mipi-apps-base-x86_64.systemd.preset
  mipi-apps-bluetooth.systemd.preset
  mipi-apps-mobile.systemd.preset
  mipi-gnome.systemd.preset
  setup
  sudoers
  timesyncd.conf
)
sha256sums=('9763fc5ed98790a6afbec735fbd95fc879daef4cc6c9bcf5885f922e7c1e57a4'
            '04360750cad35709c95cbf0a5a54977743f6e0f0a432c0e07905a853dc80980e'
            'b25b63554de5bdc78b39c6e74fbc200e0bd30ac1daf81d9e6ee7769525a9d3c2'
            '870baa38ef9ca351edca5ca3bbd53a262cc588bcaa4d4264fc04e193c766b024'
            '51eec44d50e4f6ec4ba887f1420077e60170a936f71d1a677bbab90b5d4b4bcd'
            '536639b0e57869679a2b66afeef3cb3c375adf8b12a7ad04846183d6cdde53aa'
            '73429d55fce7decce143141c3c6658d9833459ddb4eacef52e2dabf6aaa762e0'
            '9ddf444cfa76a64fc20c100f0122634e37725fc016c0810c7b86a4ec968a48d8'
            '37db54711cc3c38cfc8b993af360da85916cd11177ff2d0ae00cb8bdfd3b35c4'
            'a8af1817780709a561ef297cf0850e9a3dfaa0d88bfac05518878b6f946618c6'
            '2dd89ff732a21142dabe3ee4c04b33ddcd3478c3a77b9809632d38ba6b297f33'
            '503a7424f267a4d73954857975b8e8da8f4f156df1ecd44a0f4210e9b125c2e3'
            'bea5b7f57fac9f0177f3a5d7ba2958317ad67832a78e0095af7693b8e81295bb')

package_mipi-gnome() {
  # GNOME (almost the complete group). Each package must be listed explicitly
  # since it is not possible to add a group as dependency in PKGBUILD
  depends=(baobab evince gdm gnome-backgrounds gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-color-manager gnome-connections gnome-contacts gnome-control-center gnome-disk-utility gnome-font-viewer gnome-keyring gnome-logs gnome-maps gnome-menus gnome-photos gnome-remote-desktop gnome-session gnome-settings-daemon gnome-shell gnome-shell-extensions gnome-system-monitor gnome-terminal gnome-text-editor gnome-user-docs gnome-user-share gnome-weather grilo-plugins gvfs gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb loupe nautilus rhythmbox simple-scan snapshot sushi tecla totem tracker3-miners xdg-desktop-portal-gnome xdg-user-dirs-gtk yelp)
  
  # GNOME extra (selected packages)
  depends+=(dconf-editor gnome-themes-extra gnome-tweaks)
  
  # GNOME extensions
  depends+=(
    gnome-shell-extension-arch-update
    gnome-shell-extension-blur-my-shell
    gnome-shell-extension-forge-git
    gnome-shell-extension-just-perfection-desktop
    gnome-shell-extension-top-bar-organizer
    gnome-shell-extension-vitals
  )

  # Systemd presets
  install -Dm0644 mipi-gnome.systemd.preset "${pkgdir}"/usr/lib/systemd/system-preset/10-mipi-gnome.preset

  # GNOME configuration
  install -Dm0644 dconf-profile "${pkgdir}"/etc/dconf/profile/user
  install -Dm0644 dconf-00_extensions "${pkgdir}"/etc/dconf/db/local.d/00_extensions
  install -Dm0644 dconf-10_theming "${pkgdir}"/etc/dconf/db/local.d/10_theming
  install -Dm0644 dconf-20_keybindings "${pkgdir}"/etc/dconf/db/local.d/20_keybindings
  install -Dm0644 dconf-30_misc "${pkgdir}"/etc/dconf/db/local.d/30_misc
}

package_mipi-apps-base() {
  pkgdesc="Software packages that are needed for each installation"

  # Base stuff
  depends=(base linux-firmware logrotate man-db man-pages nano pacman-contrib s-nail usbutils)
  # Note: - cmake is needed since the vterm package for emacs requires that (some
  #         parts are being built upon first use)
  #       - ripgrep is required by ripgrep search in emacs
  depends+=(autoconf automake bison cmake devtools flex gcc m4 make patch pkgconf ripgrep sudo texinfo which)

  # Other basic stuff
  depends+=(gnupg seahorse udisks2)
  
  # Networking
  depends+=(avahi networkmanager)
  
  # Tools
  depends+=(deja-dup gparted lsb-release rsync yay)
  depends_x86_64=(bitwarden reflector veracrypt)

  # Terminal & shell
  depends+=(fastfetch zsh)

  # Printer & scanner stuff
  depends+=(cups cups-filters cups-pdf cups-pk-helper hplip libcups python-pillow python-pycups system-config-printer wget)

  # Theming
  depends+=(numix-icon-theme-git)
  
  # Applications
  # --- Editors
  depends+=(emacs)
  # --- Internet
  depends+=(chromium firefox firefox-i18n-de)
  # --- Mail
  depends+=(thunderbird thunderbird-i18n-de)
  # --- Office
  depends+=(libreoffice-fresh libreoffice-fresh-de hunspell hunspell-de hyphen hyphen-de libmythes mythes-de) 
  # --- misc
  depends+=(repman) 

  # Enable sudo
  install -Dm0644 sudoers "${pkgdir}"/etc/sudoers.d/10-mipi
  # Systemd presets
  install -Dm0644 mipi-apps-base-any.systemd.preset "${pkgdir}"/usr/lib/systemd/system-preset/10-mipi-apps-base-any.preset
  if [ $CARCH = "x86_64" ]; then
      install -Dm0644 mipi-apps-base-x86_64.systemd.preset "${pkgdir}"/usr/lib/systemd/system-preset/20-mipi-apps-base-x86_64.preset
  fi
  # Setup script for user mipi
  install -Dm0755 setup "${pkgdir}"/usr/bin/setup-mipi
  # Configure systemd timesyncd    
  install -Dm0755 timesyncd.conf "${pkgdir}"/etc/systemd/timesyncd.conf.d/10-mipi.conf
  
  install=mipi-apps-base.install
}

package_mipi-apps-bluetooth() {
  pkgdesc="Packages for an installation of Bluetooth support"

  depends=(mipi-apps-base)

  # bluetooth
  depends+=(bluez bluez-utils)

  install -Dm0644 mipi-apps-bluetooth.systemd.preset "${pkgdir}"/usr/lib/systemd/system-preset/10-mipi-apps-bluetooth.preset

  install=mipi-apps-bluetooth.install
}

package_mipi-apps-development() {
  pkgdesc="Software packages that are needed for development activities"

  depends=(mipi-apps-base)

  # Golang
  depends+=(go golangci-lint)
  # misc
  depends+=(asciidoctor git hugo reuse visual-studio-code-bin)
}

package_mipi-apps-mobile() {
  pkgdesc="Packages for an installation on a mobile device"

  depends=(mipi-apps-base)

  # display management
  depends+=(tlp tlp-rdw)

  install -Dm0644 mipi-apps-mobile.systemd.preset "${pkgdir}"/usr/lib/systemd/system-preset/10-mipi-apps-mobile.preset

  install=mipi-apps-mobile.install
}

package_mipi-apps-multimedia() {
  pkgdesc="Software packages that are needed for multimedia activities"

  depends=(mipi-apps-base)

  # Books
  depends+=(calibre)
  # Music
  depends+=(easytag puddletag)
  # Pictures / graphics
  depends+=(darktable gimp gimp-help-de gimp-plugin-gmic hugin)
  # Videos
  depends+=(mediaelch otr)
}
