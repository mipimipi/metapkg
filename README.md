# metapkg 

[Meta packages](https://wiki.archlinux.org/index.php/Meta_package_and_package_group) that I use to install my machines.

## License

[GNU Public License v3.0](https://github.com/mipimipi/metapkg/blob/master/LICENSE)
